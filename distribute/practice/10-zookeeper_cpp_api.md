## ZK C API

### Сборка примеров

```asciidoc
$ cd zk_example_api && mkdir build && cd build &&  cmake .. && make
```

Сборка по одному:
```
g++ -std=c++11 sync_api.cpp -o sync_api -lzookeeper_mt
```
На выходе получаем бинарник sync_api

Запуск:
```
./sync_api --cluster mipt-node01:2181 --root /some_znode
```

Аналогично для остальных примеров.

#### Код на кластере: `/home/velkerr/seminars/pd2018/17-zookeeper_c_api`
#### [Документация](https://zookeeper.apache.org/doc/r3.5.6/zookeeperProgrammers.html), [Исходники](https://github.com/apache/zookeeper/blob/master/zookeeper-client/zookeeper-client-c/src/zookeeper.c)

### соединение

Работа начинается с создания zhandle_t, через который вы будете работать с zk. Создание влечет за собою
создание управляющего потока и начало установления сесии. 

```cpp
zhandle_t *zookeeper_init(const char *host, watcher_fn watcher,
        int recv_timeout, const clientid_t *clientid, void *context, int flags)
```

Получать сообщения о смене состояния кластера можно через обобщённую функцию вотчера:

```cpp
typedef void (*watcher_fn)(zhandle_t *zh, int type,
        int state, const char *path,void *watcherCtx);
```

где:
  * `type`  -- тип события, например `ZOO_CREATED_EVENT`
  * `state` -- состояние соединения. `ZOO_EXPIRED_SESSION_STATE`
  * `watcherCtx` -- объект `context` переданный в `zookeeper_init`
  
Вотчер, переданный в `zookeeper_init` полезен в первую очередь для детектирования потери соединения и рестарта логики.
Но это не единственный вариант. Обычные методы начнут возвращать `ZEXPIREDSESSION`, а потом `ZINVALIDSTATE` при работе
с протухшим соединением.
  
Каждый `zookeeper_init` должен иметь соответствующий `zookeeper_close(zhandle_t *zh)` если вы хотите
поскорее разрушить ваши эфемерные ноды и не хотите спорить с ASAN.



### Синхронный API

Разберите `sync_api.cpp`

Обратите внимание, что:
1. в синхронные методы передаются через аргументы контейнеры для записи результата. Например:
   ```cpp
   ZOOAPI int zoo_get(zhandle_t *zh, const char *path, int watch, char *buffer,
                      int* buffer_len, struct Stat *stat);
   ```
2. Можно заглянуть в реализацию, синхронные методы сделаны поверх асихронных аналогов
3. Есть опеделённые проблемы с порядком получения ответов при смешении синхронных и асинхронных вызов. (в java то же самое)
   Наример, если вы задаёте 2 асинхронные операции Aop1 и Aop2 и при обработке коллбека первой операции делаете синхронный
   выхов Sop1, то программа обработает ответы в порядке Aop1 Sop1 Aop2, а с тз модели ZK верный порядок Aop1 Aop2 Sop1
   
   Правило большого пальца: Не мешайте (или мешайте с умом) синхронные и асинхронные операции. Обычно инициализация синхронная,
   а дальше идёт набор синхронных и асинхронных блоков.

### Асинхронный API

Разберите файл `async_api.cpp`

В целом сигнатура асинхронных методов отличается от синхронной отсутствием переменных для записи результата,
зато вместо них передаётся функция, которую нужно запустить при получении результата.

```cpp
int zoo_aget(zhandle_t *zh, const char *path, int watch, data_completion_t dc,
        const void *data)
```

Сигнатуры completion методов:

```cpp
// Async delete -- пустой коллбек
typedef void (*void_completion_t)(int rc, const void *data);

// Async exists: получаем Stat или rc==ZNONODE
typedef void (*stat_completion_t)(int rc, const struct Stat *stat,
        const void *data);

// Async get -- данные и stat
typedef void (*data_completion_t)(int rc, const char *value, int value_len,
        const struct Stat *stat, const void *data);

// Список строк (дети узла, например)
typedef void (*strings_completion_t)(int rc,
        const struct String_vector *strings, const void *data);

// Async Create: имя созданного узла + Stat
typedef void (*string_stat_completion_t)(int rc,
        const char *string, const struct Stat *stat, const void *data);

// String_vector + Stat
typedef void (*strings_stat_completion_t)(int rc,
        const struct String_vector *strings, const struct Stat *stat,
        const void *data);
```

Во всех коллбеках `const void* data` это то, что вы передали как `*data` в `zoo_aget` и аналоги.

**Важно помнить про коллбеки**:

1. По своей модели ZK обязан доставить ответы в порядке отправки запросов
2. Единственное, что он может сделать -- запустить 1 поток и обрабатывать в нём **последовательно** ответы, т.к. кроме
   завершения callback другой возможности понять, что сообщение доставлено -- нет.
3. Поэтому не стоит спать или майнить биткоин в коллбеках
4. И не забудьте про thread safety!

**Важно помнить про асинхронные методы**:
1. Ошибка может вернуться немедленно (например при протухшем `zh`), а запрос не будет отправлен. Не забудьте обработать
   этот сценарий.



Так же, можно удобно избавиться от коллбеков в пользу привычной пары `promise/future`

Создаем объект для хранения результата:

```c++
template<typename T>
struct TAsyncResult {
  int error;
  T value;

  bool Success() {
    return error == ZOK;
  }
};
```

И делаем обёртку вокруг `zoo_acreate`

```c++
std::future<TAsyncResult<std::string>> Create(
  const std::string &path,
  const std::string &data,
  int flags
) {
    using ThisPromise = std::promise<TAsyncResult<std::string>>;
    auto args = new ThisPromise;
    auto result = args->get_future();
    
    
    auto callback = [](int rc, const char *value, const void *data) {
      auto theState = (ThisPromise *)(data);
      std::string string_value;
      if (rc == ZOK) {
        string_value.assign(value);
      }
      theState->set_value({rc, std::move(string_value)});
      delete theState;
    };
    
    int ret = zoo_acreate(
        zh_,
        path.data(),
        data.data(),
        static_cast<int>(data.size()),
        &ZOO_OPEN_ACL_UNSAFE,
        flags,
        callback,
        args
    );
    
    if (ret != ZOK) {
      args->set_value({ret, std::string{}});
      delete args;
    }
    return result;
}

```


### Watches API
  

У методов есть их двойники, куда можно передать коллбек для вотчера: `zoo_aget -> zoo_awget` или `zoo_get -> zoo_wget`

Отличаются они новой парой опций:

* `watcher_fn watcher` -- коллбек
* `void* watcherCtx` -- традиционные параметры коллбека

На `watcher_fn` мы помотрели, когда рассматривали `zookeeper_init`. Фактически zookeeper позволяет вам привязать к
событию специализированную функцию - обработчик.

Разберите файл `watches_api.cpp`

Можно расскомментировать строку в NodeCache::WatcherReal и посмотреть, что будет:
```cpp
// std::this_thread::sleep_for(std::chrono::seconds{6});
```


### Multi OP

Multi Op это вектор операций. Позволяет атомарно применить себя или быть откаченным.

Подробный пример в файле `tx_api.cpp`

тезисно это так выглядит:
```cpp
zoo_op_t ops[2];
  zoo_op_result_t ops_result[2];
  std::string rbufs[2];
  rbufs[0].resize(3000);
  rbufs[1].resize(3000);

  std::string path_template = root + "/item-";
  zoo_create_op_init(
      &(ops[0]),
      path_template.data(),
      nullptr,
      0,
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_EPHEMERAL_SEQUENTIAL,
      (char*) rbufs[0].data(),
      rbufs[0].capacity()
    );
  zoo_create_op_init(
      &(ops[1]),
      path_template.data(),
      nullptr,
      0,
      &ZOO_OPEN_ACL_UNSAFE,
      ZOO_EPHEMERAL_SEQUENTIAL,
      (char*) rbufs[1].data(),
      rbufs[1].capacity()
  );

  auto rv = zoo_multi(
      zh,
      2,
      ops,
      ops_result
    );
  assert(rv == ZOK);
```

В качестве результата вы получите `rv`, который позволит вам понять поизошла ли транзакия. Так же можно посмотреть .error
у операций и найти виновную. Сигнатура предельно похожа на синхронные методы.



## Задачи на семинар

### Priority Queue
Попробуйте реализовать на ZK очередь Single Producer Single Consumer с приоритетами. Интерфейс:
1. `put(str task_id, int piority, payload)`
2. `get() -> (task_id, piority, payload)`
    Блокирующий выхов получения новых данных
3. `consume()`

`consume()` вызывает консьюмер после обработки и уже это триггерит удаление узла в zk