## Семинар: MapReduce-2 (Распределеная часть)

Заходим на `par2018XX@mipt-client.atp-fivt.org -L 8088:mipt-master:8088`.

В случае проблем с доступами или ключами заходим по логину `hdfsuser` (пароль `hdfsuser`). Это не желательно т.к. у вас в этом случае будет общая очередь на кластере.

### Обёртка над HDFS shell

Стандартный HDFS shell.
* не хранит состояние
* на каждую команду стартует своя Java Virtual Machine,
* нет автоподстановки,
* нельзя сменить пользователя (su ...).
 
В 2017 г. в Avast разработали обрёртку над HDFS shell: https://github.com/avast/hdfs-shell .
Написана на Java, но с точки зрения пользователя - bash.
 
Простая установка:
* качаем бинарник (напр., wget'ом),
* разрахивируем (unzip), добавляем ./libs в CLASSPATH
* заходим в hdfs-shell-1.0.7/bin
* запускаем hdfs-shell.sh.

На кластере оболочка установлена, просто запустите `hdfs-shell`.
 
Pros.
* Cокращённые названия команд (например, ls, а не hdfs dfs -ls). Подробнее - выполните help.
* Есть автоподстановка,
* Есть дополнительные команды, которые есть в bash, но нет в HDFS shell (напр., pwd, groups).
* Можно создавать свои команды и встраивать их в обёртку.

Cons.
* Нельзя выполнять цепочки команд (например, ls -l | grep ).
* Через `su` можно сменить пользователя без авторизации => небезопасно. 

### Структура MapReduce-task (Map, Reduce)

```
public void run(Context context) {
    setup();
    while (keys.next()) {
        reduce(key, value);
    cleanup();
```

Посмотрим на пример WordCount:
```
current_key = None
sum_count = 0
for line in sys.stdin:
    try:
        key, count = line.strip().split('\t', 1)
        count = int(count)
    except ValueError as e:
        continue
    if current_key != key:
        if current_key:
            print "%s\t%d" % (current_key, sum_count)
        sum_count = 0
        current_key = key
    sum_count += count
if current_key:
    print "%s\t%d" % (current_key, sum_count)
```

Где здесь стадии setup(), cleanup(), run()?

### Счётчики в MapReduce
 - ещё 1 возможность отладки
 - возможность в некоторых случаях избежать редьюсера.
 
Исходник: `/home/velkerr/seminars/pd2018/06-counters`

Объявление счётчика в Python3: `print("reporter:counter:Point stats,Inside points,{}".format(is_inside), file=sys.stderr)`

### Задача
Оценить значение числа PI методом Монте Карло. 
Исходник: `/home/velkerr/seminars/pd2018/07_pi_stub`

## Hadoop Java API

Исходник: `seminars/pd2018/05_wordcount_java`.

#### Основной класс

`public class WordCount extends Configured implements Tool {...}`

* Класс WordCount содержит всю логику задачи
* Базовый класс [Configured](https://hadoop.apache.org/docs/r2.4.1/api/org/apache/hadoop/conf/Configured.html) отвечает за возможность получить конфигурацию HDFS (достаточно вызвать `getConf()` внутри `run()`). Это полезно в тех случаях, когда нужно работать с HDFS из программы (например, удалять промежуточные результаты),
* [Tool](https://hadoop.apache.org/docs/stable/api/org/apache/hadoop/util/Tool.html) - интерфейс, содержащий единственный метод `run()`, который (а) парсит аргументы командной строки, (б) производит настройку Job'ы. Выполняется `run()` **на клиенте**.

#### Метод `run()`. Важные моменты.

1. Форматы ввода-вывода:
```java
job1.setInputFormatClass(TextInputFormat.class);
job1.setOutputFormatClass(TextOutputFormat.class);
```
Это форматы, в которых пишется результат Job'ы. В Hadoop есть несколько встроенных форматов ввода-вывода. Основные:
**TextInputFormat / TextOutputFormat**
Результат пишется в стандартный текстовый файл.
Плюсы: быстро работает.
Минусы: теряет данные о типах ключей-значений. При считывании данных будем иметь пары `(LongWritable offset, Text rawString)`. rawString нужно парсить.

**KeyValueTextInputFormat / KeyValueTextOutputFormat**
Улучшенная версия предыдущего. Типы ключей-значений хранит только если это простые типы (например, IntWritable, Text).

**SequenceFileInputFormat / SequenceFileTextOutputFormat**
Плюсы: хранит типы ключей-значений.
Минусы: Выходной файл будет записан в формате бинарного файла и прочитать его командой `hdfs dfs -cat` не выйдет.

В итоге:
* в промежуточных job'ах лучше использовать форматы KeyValue или SequenceFile.
* В последней job'е - TextOutputFormat

Любой Input / OutputFormat позволяет задавать сжатие. Это сэкономит затраты на передачу по сети.

```java
SequenceFileOutputFormat.setOutputCompressionType(job, CompressionType.BLOCK);
SequenceFileOutputFormat.setCompressOutput(job, true);
```

CompressionType - механизм сжатия. Бывает `BLOCK` (сжимаем поблочно) и `RECORD` (сжимаем записи раздельно).

*CompressionType:  Тем, кто слушает Java, полезно посмотреть на хорошую реализацию `enum` с полями и методами.*

[Больше](http://timepasstechies.com/input-formats-output-formats-hadoop-mapreduce/) входных и выходных форматов.

2. Задание кол-ва мапперов и редьюсеров.

`job.setNumReduceTasks(8);` - задаем кол-во редьюсеров.
Число мапперов напрямую задать нельзя. Система устанавливает его равным кол-ву сплитов.
* Размер сплита можно изменять (по умолчанию равен размеру блока)
* В логах Hadoop-задачи можно найти строчку: `19/10/14 21:37:44 INFO mapreduce.JobSubmitter: number of splits:2`.

#### Мапперы и редьюсеры

Соoтветствуют паттерну, см. [выше](/distribute/practice/03-hadoop2.md#%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%82%D1%83%D1%80%D0%B0-mapreduce-task-map-reduce). В отличие от Hadoop Streaming, цикл обхода сплита писать не надо.
Выполняются на нодах.

#### Классы-обёртки
В Hadoop существует 2 интерфейса: `Writable` и `WritableComparable`. Оба они поддерживают сериализацию / десериализацию, нужную для передачи данных между нодами.
* Типы входных-выходных ключей должны реализовывать интерфейс `WritableComparable` (т.к. их придётся сравнивать на этапе сортировки).
* Для значений достаточно `Writable`.
* Классы-обёртки, входящие в поставку Hadoop, реализовывают интерфейс `WritableComparable`.

#### Сборка и запуск программмы
Собирать Java-проект можно с помощью ant или maven. См. соответственно build.xml и pom.xml в `/home/velkerr/seminars/pd2018/05_wordcount_java`.
Запуск программы: `hadoop jar <путь_к_jar> <полное_имя_главного_класса> <вход> <выход> [другие_аргументы (напр. кол-во редьюсеров)]`

### Задача
1. Добавим combiner в WordCount. Для этого в настройках job'ы (`run()`) добавляем метод `setCombinerClass`. Подходит ли редьюсер на роль combiner'a? Сравним быстродействие работы программы с combiner и без.
2. Внесём изменения в WordCount:
   * не учитывать слова короче 4 символов (см. стоп-слова).
   * выводить в результат только те слова, у которых кол-во встречаемости > 4. 

## Глобальная сортировка
Внутри выдачи редьюсера сортировка соблюдается. Как сделать общую сортировку для всех редьюсеров? 
* Лобовое решение: пройтись по всем ключам и явно сказать Partitioner'у, какие ключи попадут на тот или иной редьюсер. Это долго.
* Выход - семплирование. Проходим не все ключи, а выбираем с некоторой вероятностью, затем аппроксиимруем на весь датасет.

Пример кода: `InputSampler.Sampler<LongWritable, Text> sampler = new InputSampler.RandomSampler<>(0.5, 10000, 10);`. 
* 0,5 - вероятность выбора записи:
* максимальное кол-во "выборов"
* максимальное кол-во сплитов.

Как только дошли до границы хотя бы по одному аргументу, семплирование прекращаем.

Такой Sampler подаётся в TotalOrderPartitioner. Подробнее см. "Hadoop. The definitive guide, 4 изд. стр. 287".
Пример WordCount с глобальной сортировкой: `/home/velkerr/seminars/pd2018/10-globalsort`.

### Задача
Поэкспериментируем с параметрами InputSampler. Например, уменьшим вероятность до 0,2. Если данных мало, Sampler не сможет сформировать выборку и программа упадёт.

### Joins
Определите число пользователей stackoverflow, которые задавали вопросы с FavoriteCount >= 100 и давали при этом правильные ответы, т.е. с наибольшим Score для вопроса. Для подсчета результирующего числа пользователей можно использовать счетчики (Hadoop counters).
Входные данные: посты stackoverflow.com
Формат вывода: идентификаторы пользователей, по одному на строку
На печать: число пользователей
 